const {Router} = require('express');
const controller = require('../controllers/index.controller');
const routes = Router();

routes.get('/', controller.getdata);
module.exports = routes;
